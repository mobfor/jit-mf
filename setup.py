from setuptools import setup, find_packages


with open('requirements.txt') as fp:
    install_requires = fp.read()

setup(
    name="jitmf",
    version='0.1',
    packages=find_packages(),
    py_modules=['jitmf'],
    install_requires=install_requires,
    entry_points='''
        [console_scripts]
        jitmf=jitmf.console.cli:entrypoint
    ''',
)