import time
class Case:
    apk_name = None
    device_arch = None
    tmp_path = None
    frida_gadget_path = None
    decompiled_apk_dir = None
    hook_script_path = None
    device_hook_dir = None
    attached_device_id = None
    output_path = None
    time_started=None

    def __init__(self, apk_name, tmp_path, device_id, output_path):
        self.apk_name = apk_name
        self.tmp_path = tmp_path
        self.attached_device_id = device_id
        self.output_path = output_path
        self.time_started = time.strftime("%Y%m%d_%H%M%S")
