import delegator
import os
import time
import click
import zipfile
import re
import itertools
from time import sleep
from datetime import datetime

import xml.etree.ElementTree as ElementTree

from jitmf.utils.Case import Case
from subprocess import list2cmdline

JITMF_FILENAME_PATTERN='*_dump_*.hprof'


delegator_timeout=120
permissions = {
    'internet': 'android.permission.INTERNET',
    'sd_read': 'android.permission.READ_EXTERNAL_STORAGE',
    'sd_write': 'android.permission.WRITE_EXTERNAL_STORAGE'}
case = None

def pull_apks(dir: str, case: Case):
    click.secho('Pulling apks...', dim=True)
    o = execute_adb_command("adb -s "+case.attached_device_id+" shell pm list packages | awk -F':' '{print $2}'",case)
    pkg_list = o.out.strip().splitlines()
    pkg_ctr=0
    
    for pkg in pkg_list:
        pkg_ctr += 1
        o = execute_adb_command("adb -s "+case.attached_device_id+" shell pm path "+pkg+" | awk -F':' '{print $2}'",case)
        path = o.out.strip().splitlines()[0]
        o = execute_adb_command('adb -s '+case.attached_device_id+' pull '+path+' '+dir,case)
        delegator.run('mv '+dir+'/base.apk '+dir+'/'+pkg+'.apk' ) #rename apk in case it's base to actual app name
        click.secho('Pulled {0}. {1} out of {2} downloaded'.format(pkg,pkg_ctr,len(pkg_list)), dim=True)

    
def pull_dumps(dir: str, case: Case):
    click.secho('Pulling JIT MF dumps...', dim=True)
    o = execute_adb_command("adb -s "+case.attached_device_id+" shell echo \\$EXTERNAL_STORAGE",case)  # get location of sd card
    sdcard_path = o.out.strip()
    o = execute_adb_command("adb -s "+case.attached_device_id+" shell 'ls "+sdcard_path+"/"+JITMF_FILENAME_PATTERN+"' | tr -d '\r' | xargs -i adb pull {} "+dir,case)

def _copy_meta_inf(case: Case):
    meta_inf = os.path.join(case.decompiled_apk_dir, 'original', 'META-INF')
    standard_files = re.compile(r'^(?:[A-Z0-9_-]+\.(?:RSA|SF)|MANIFEST\.MF)$')
    extra_names = list(itertools.filterfalse(standard_files.match, os.listdir(meta_inf)))
    if extra_names:
        click.secho('Appending {0} extra entries in META-INF to the APK...'.format(len(extra_names)))
        with zipfile.ZipFile(os.path.join(case.tmp_path,'.patched.apk'), 'a', zipfile.ZIP_DEFLATED) as apk:
            for extra_name in extra_names:
                full_path = os.path.join(meta_inf, extra_name)
                if os.path.isdir(full_path):
                    prefix_len = len(full_path) + 1 # trailing '/'
                    for dirpath, _, filenames in os.walk(full_path):
                        for filename in filenames:
                            src_name = os.path.join(dirpath, filename)
                            dest_name = 'META-INF/{dirname}/{path}'.format(
                                    dirname=extra_name,
                                    path=src_name[prefix_len:].replace(os.sep, '/'))
                            apk.write(src_name, dest_name)
                else:
                    apk.write(full_path, 'META-INF/' + extra_name)

def _get_android_manifest() -> ElementTree:
    """
        Get the AndroidManifest as a parsed ElementTree
        :return:
    """

    # use the android namespace
    ElementTree.register_namespace('android', 'http://schemas.android.com/apk/res/android')

    return ElementTree.parse(os.path.join(case.decompiled_apk_dir, 'AndroidManifest.xml'))   

def get_device_arch(case: Case):
    o = execute_adb_command('adb  -s '+case.attached_device_id+'  shell getprop ro.product.cpu.abi',case)
    # o = delegator.run('adb  -s '+case.attached_device_id+'  shell getprop ro.product.cpu.abi')
    arch = o.out.strip()#catering for arm64-v8a cases 

    return arch

def modify_extract_libs(c: Case):
    xml = _get_android_manifest()
    root = xml.getroot()
    for a in root.iter('application'): 
        click.secho('Setting extract Native libs to true', fg='green')
        a.attrib["{http://schemas.android.com/apk/res/android}extractNativeLibs"] = 'true'
        
    xml.write(os.path.join(c.decompiled_apk_dir,'AndroidManifest.xml'), encoding='utf-8', xml_declaration=True)


def inject_permissions(c: Case, p):
    global case
    case = c
    
    for perm in p:
        permission = permissions[perm]
        o = delegator.run(list2cmdline([
            'grep',
            permission,
            os.path.join(c.decompiled_apk_dir,'AndroidManifest.xml')
        ]), timeout=delegator_timeout) 

        # if the app already has the internet permission, easy mode :D
        if permission in o.out:
            click.secho('App already has {0}'.format(permission), dim=True)
        else:
            click.secho('Adding permission {0} to app'.format(permission), fg='green')

            xml = _get_android_manifest()
            root = xml.getroot()

            click.secho('Injecting permission: {0}'.format(permission), fg='green')

            # prepare a new 'uses-permission' tag
            child = ElementTree.Element('uses-permission')
            child.set('android:name', permission)
            root.append(child)

            click.secho('Writing new Android manifest...', dim=True)

            xml.write(os.path.join(c.decompiled_apk_dir,'AndroidManifest.xml'), encoding='utf-8', xml_declaration=True)

def _determine_smali_path(activity: str, decompiled_dir: str):
    dirs = activity.split('.')
    activity_name = dirs[-1]+'.smali'

    o = delegator.run(list2cmdline([
        'find',
        decompiled_dir,
        '-name',
        activity_name
    ]), timeout=delegator_timeout) 

    click.secho('> LaunchActivity may be in: {0}'.format(o.out.strip()), fg='green', bold=True)
    launch_activity_path=o.out.strip() 
    return launch_activity_path

def _get_appt_output(apk_source: str):
    """
        Get the output of `aapt dump badging`.
        :return:
    """

    o = delegator.run(list2cmdline([
        'aapt',
        'dump',
        'badging',
        apk_source
    ]), timeout=60*5)

    if len(o.err) > 0:
        click.secho('An error may have occurred while running aapt of source {0}.'.format(apk_source), fg='red')
        click.secho(o.err, fg='red')
    
    return o.out

def execute_adb_command(cmd: str, c: Case):
    with open(os.path.join(c.output_path,'logs','runtime_log_'+c.time_started+'.log'), 'a') as file_object:        
        file_object.write(cmd+"\n")
    o = delegator.run(cmd)
    return o


def _get_launchable_activity(source: str) -> str:
    activity = ''
    aapt = _get_appt_output(source).split('\n')

    for line in aapt:
        if 'launchable-activity' in line:
            activity = line.split('\'')[1]

    # If we got the activity using aapt, great, return that.
    if activity != '':
        return activity

    # if we dont have the activity yet, check out activity aliases
    click.secho('Unable to determine the launchable activity using aapt, trying to manually parse the AndroidManifest for activity aliases...', dim=True, fg='yellow')

    # Try and parse the manifest manually
    manifest = _get_android_manifest()
    root = manifest.getroot()

    # grab all of the activity-alias tags
    for alias in root.findall('./application/activity-alias'):

        # Take not of the current activity
        current_activity = alias.get('{http://schemas.android.com/apk/res/android}targetActivity')
        categories = alias.findall('./intent-filter/category')

        # make sure we have categories for this alias
        if categories is None:
            continue

        for category in categories:

            # check if the name of this category is that of LAUNCHER
            # its possible to have multiples, but once we determine one
            # that fits we can just return and move on
            category_name = category.get('{http://schemas.android.com/apk/res/android}name')

            if category_name == 'android.intent.category.LAUNCHER':
                return current_activity

    # getting here means we were unable to determine what the launchable
    # activity is
    click.secho('Unable to determine the launchable activity for this app.', fg='red')
    raise Exception('Unable to determine launchable activity')

