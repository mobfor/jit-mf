from datetime import datetime

class SuspFinding:
    # time = None
    suspicious_string = None
    matched_string = None
    event = None



    @property
    def time(self):
        return self.__time

    @time.setter
    def time(self, time):
        self.__time = datetime.fromtimestamp(int(time)/1000.0) 
        

    
    def __list__(self):
        t = datetime.fromtimestamp(int(self.time)/1000.0).strftime('%Y-%m-%d %H:%M:%S')
        return [t, self.suspicious_string, self.matched_string, self.event]
        # return {"t":t, "s": self.suspicious_string, "s2":self.matched_string}

    def __repr__(self):
    
        t = datetime.fromtimestamp(int(self.time)/1000.0).strftime('%Y-%m-%d %H:%M:%S')
        return t+','+self.event+','+self.suspicious_string+','+self.matched_string 
        # return 'Time captured: %s, Suspicious string: %s, Captured string: %s, Event %s' % (t, self.suspicious_string, self.matched_string, self.event)     
        # return [t, self.suspicious_string, self.matched_string]

  
    
    # def __init__(self, apk_name, tmp_path):
    #     self.apk_name = apk_name
    #     self.tmp_path = tmp_path
