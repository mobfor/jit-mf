import click
import delegator
from subprocess import list2cmdline
import fileinput

delegator_timeout=120

def _determine_end_of_smali_method_from_line(smali: list, start: int) -> int:
    """
        Determines where the .end method line is.
        This method is also aware of a methods that 'returns' and will
        return the line before that too.
        :param smali:
        :param start:
        :return:
    """

    # enumerate all of # the lines in the original smali sources and mark the offsets of the
    # lines that contain '.end method'. the search starts right after the
    # original inject marker so that we can pick the top most .end method
    # when we are done searching. this is also why the # represented in the
    # inject marker is added to the calculated marker in the list of end methods.
    end_methods = [(i + start) for i, x in enumerate(smali[start:]) if '.end method' in x]

    # ensure that we found at least one .end method
    if len(end_methods) <= 0:
        raise Exception('Unable to find the end of the existing constructor')

    # set the last line of the constructors method to the one
    # just before the .end method line
    end_of_method = end_methods[0] - 1

    # check if the constructor has a return type call. if it does,
    # move up one line again to inject our loadLibrary before the return
    if 'return' in smali[end_of_method]:
        end_of_method -= 1

    return end_of_method

def _patch_smali_with_load_library(smali_lines: list, inject_marker: int) -> list:
    """
        Patches a list of smali lines with the appropriate
        loadLibrary call based on wether a constructor already
        exists or not.
        :param smali_lines:
        :param inject_marker:
        :return:
    """

    # raw smali to inject.
    # ref: https://koz.io/using-frida-on-android-without-root/

    # if no constructor is present, the full_load_library is used
    full_load_library = ('.method static constructor <clinit>()V\n'
                            '   .locals 0\n'  # _revalue_locals_count() will ++ this
                            '\n'
                            '   .prologue\n'
                            '   const-string v0, "frida-gadget"\n'
                            '\n'
                            '   invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V\n'
                            '\n'
                            '   return-void\n'
                            '.end method\n')

    # if an existing constructor is present, this partial_load_library
    # will be used instead
    partial_load_library = ('\n    const-string v0, "frida-gadget"\n'
                            '\n'
                            '    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V\n')

    # Check if there is an existing clinit here. If there is, then we need
    # to determine where the constructor ends and inject a simple loadLibrary
    # just before the end
    if 'clinit' in smali_lines[inject_marker]:
        click.secho('Injecting into an existing constructor', fg='yellow')

        end_of_constructor = _determine_end_of_smali_method_from_line(smali_lines, inject_marker)
        click.secho('Injecting loadLibrary call at line: {0}'.format(end_of_constructor), dim=True, fg='green')

        patched_smali = \
            smali_lines[:end_of_constructor] + partial_load_library.splitlines(keepends=True) + \
            smali_lines[end_of_constructor:]

    else:

        # if there is no constructor, we can simply inject a fresh constructor
        click.secho('Injecting loadLibrary call at line: {0}'.format(inject_marker), dim=True, fg='green')

        # inject the load_library code between
        patched_smali = \
            smali_lines[:inject_marker] + full_load_library.splitlines(keepends=True) + smali_lines[inject_marker:]

    return patched_smali

def _revalue_locals_count(patched_smali: list, inject_marker: int):
    """
        Attempt to ++ the first .locals declaration in a list of
        smali lines confined to the same method.
        :param patched_smali:
        :param inject_marker:
        :return:
    """

    def _h():
        click.secho('Could not update .locals value. Sometimes this may break things,'
                    'but not always. If the applications crashes after patching, try '
                    'and add the --pause flag, fixing the patched smali manually.', fg='yellow')

    # next, update the .locals count (if its defined)
    # if this step fails, its not really a big deal as many times its not
    # fatal. however, if it does fail, warn about it.
    click.secho('Attempting to fix the constructors .locals count', dim=True)
    end_of_method = _determine_end_of_smali_method_from_line(patched_smali, inject_marker)

    # check if we have a .locals declaration right after the start of our
    # already matched constructor
    defined_locals = [i for i, x in enumerate(patched_smali[inject_marker:end_of_method])
                        if '.locals' in x]

    if len(defined_locals) <= 0:
        click.secho('Unable to determine any .locals for the target constructor', fg='yellow')
        _h()
        return patched_smali

    # determine the offset for the first matched .locals definition
    locals_smali_offset = defined_locals[0] + inject_marker

    try:
        defined_local_value = patched_smali[locals_smali_offset].split(' ')[-1]
        defined_local_value_as_int = int(defined_local_value, 10)
        new_locals_value = defined_local_value_as_int + 1

    except ValueError as e:
        click.secho(
            'Unable to parse .locals value for the injected constructor with error: {0}'.format(str(e)),
            fg='yellow')
        _h()

        return patched_smali

    click.secho('Current locals value is {0}, updating to {1}:'.format(
        defined_local_value_as_int, new_locals_value), dim=True)

    # simply search / replace the integer values we already calculated on the relevant line
    patched_smali[locals_smali_offset] = patched_smali[locals_smali_offset].replace(
        str(defined_local_value_as_int), str(new_locals_value))

    return patched_smali

def _patch_smali_official_app_flag(decompiled_dir: str):
    o = delegator.run(list2cmdline([
        'find',
        decompiled_dir,
        '-name',
        '*TL_authorization.smali'
    ]), timeout=delegator_timeout) 
    auth_smali_file_path = o.out.strip()
    click.secho('** Modifying "official_app" flag for Telegram', fg='green', bold=True)

    with fileinput.FileInput(auth_smali_file_path, inplace=True) as file:
        for line in file:
            print(line.replace("or-int/lit8 v0, v0, 0x2", "or-int/lit8 v0, v0, -0x3"), end='')