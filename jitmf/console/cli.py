import os
import click
from shutil import copyfile

from jitmf.commands.packaging import patch_android_apk, deploy_to_device
from jitmf.commands.retrieval import grab_susp_apks
from jitmf.commands.analysis import analyse_dumps, load_susp_data
from jitmf.utils.Case import Case

BASE_DIR = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
ASSETS_DIR = os.path.join(BASE_DIR,'utils','assets')

@click.group()
def entrypoint():
    pass

# DOC: sets ~/.jitmf as the tmp directory to store packaged apk, gadget etc.
@entrypoint.command()
@click.option('-apk_src', required=True, help='Destination of source apk')
@click.option('-dev_id', required=True, help='Device ID from adb devices')
@click.option('-hook', required=True, help='Hooking script to inject. Parent directory is the assets/hooks folder within this tool')
@click.option('-vfrida', help='Version of Frida you''d like to work with')
@click.option('-jitmf_dir', help='Optional arg to store all generated files in the same dir')
@click.option('--install', is_flag=True, help='Install the apk')
def patchapk(apk_src, dev_id, hook, vfrida, jitmf_dir, install):
    forensic_case = Case(apk_src.split('/')[-1], os.path.join(os.path.expanduser('~'), '.jitmf_tmp'), dev_id, jitmf_dir)
    if hook.strip()=='LISTEN' or os.path.exists(hook):
        forensic_case.hook_script_path = hook.strip()
        path, case = patch_android_apk(forensic_case, apk_src, vfrida,os.path.join(forensic_case.output_path,'generated_apks'))
        if install:
            deploy_to_device(path, case)
        
        click.secho('> New patched APK can be found here: {0}'.format(path), fg='green')
    else:
        click.secho('> Hook script path does not exist. Extiting...', fg='red')
        exit

# could be improved - I'm pulling all and grepping then deleting
@entrypoint.command()
@click.option('-dir', required=False, help='Directory to store suspicious apks')
@click.option('-dev_id', required=True, help='Device ID from adb devices')
def grabapks(dir, dev_id):
    forensic_case = Case(None, None, dev_id, os.path.dirname(dir)) # the given dir is the specific susp. apks dir. The case dir is the parent dir.
    grab_susp_apks(dir, forensic_case)
    

# @entrypoint.command()
# @click.option('-suspdataf', help='Text file containing suspicious data')
# @click.option('-logpath', help='Path of output', default='/tmp/')
# @click.option('-dev_id', required=True, help='Device ID from adb devices')
# def analysedumps(suspdataf: str, logpath: str, dev_id: str) :
#     forensic_case = Case(None, None, dev_id)
#     logpath=os.path.join(logpath,'jitmf_ouput')
#     suspdata=''
#     if suspdataf:

#         suspdata = load_susp_data(suspdataf) 
    
#     analyse_dumps(logpath, suspdata, forensic_case)
