import os

from subprocess import list2cmdline
import click
import delegator

from jitmf.utils.android import pull_apks
from jitmf.utils.Case import Case

suspicious_permissions = [
    'android.permission.SEND_SMS',
    'android.permission.READ_SMS',
    'android.permission.RECEIVE_SMS',
    'android.permission.BIND_ACCESSIBILITY_SERVICE']

# stores suspicious apks in ~/.jitmf unless otherwise specified
def grab_susp_apks(dir: str, case: Case):
    susp_perm_found=False
    if dir is None:
        dir = os.path.join(os.path.expanduser('~'), '.jitmf_tmp', 'suspicious_apks')
    os.makedirs(dir, exist_ok=True)
    click.secho('Path is {0}'.format(dir), dim=True)
    
    pull_apks(dir, case)
    for _, _, files in os.walk(dir):
        click.secho('Testing {0} for suspicious permissions'.format(dir), dim=True)
        for name in files:
            susp_perm_found=False
            o = delegator.run('aapt d permissions '+os.path.join(dir,name))

            for perm in suspicious_permissions:
                if perm in o.out.strip():
                    click.secho('Permission {0} found in {1}'.format(perm, name))
                    susp_perm_found=True
                    break
            
            if not susp_perm_found:
                os.remove(os.path.join(dir, name))
    #cleanup()
    click.secho('Suspicious apks can be found in {0}'.format(dir), fg='yellow')
    