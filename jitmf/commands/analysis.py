import click
import os
import delegator
import time
import json
from datetime import datetime
from tabulate import tabulate
from jitmf.utils.SuspFinding import SuspFinding
from jitmf.utils.Writer import Writer
from jitmf.utils.ForensicTimelineElement import ForensicTimelineElement

from jitmf.utils.android import pull_dumps
from jitmf.utils.Case import Case

#loads suspicious data to string
def load_susp_data(suspfile: str):
    f = open(suspfile)
    lineList = f.read().splitlines()
    
    return lineList

def get_strings(d: str, fileName: str):

    delegator.run('strings '+os.path.join(d, fileName)+' > '+os.path.join(d,fileName+"_strings"))

    if os.stat(os.path.join(d,fileName+"_strings")).st_size > 0:
        # time.sleep(500)
        return os.path.join(d,fileName+"_strings")
    else: 
        click.secho('No strings found in the dump.', fg='red')
        return None

def checksuspiciousmatch(lines, suspdata, time, action):
    findings = []

    for l in lines:
        for susp_string in suspdata:
            if susp_string in l:
                f = SuspFinding() #initialize a finding
                f.time = time
                f.matched_string=l.strip()
                f.suspicious_string=susp_string
                f.event = action
                findings.append(f)

    return findings

# DOC: dir expl. defaults to /tmp/jitmf_output. filtered_output -> where json data is put. raw_dumps -> where strings and original dumps are
def analyse_dumps(dir: str, suspdata: str, case: Case):
    findings = []
    susp_findings = []
    raw_dumps_dir = os.path.join(dir,'raw_dumps')
    os.makedirs(raw_dumps_dir, exist_ok=True)
    pull_dumps(raw_dumps_dir,case)
    click.secho('Storing {0} log files'.format(dir), dim=True)
    for _, _, files in os.walk(raw_dumps_dir):
        for name in files:            
            if name.endswith('.hprof'):
                click.secho('Analysing log file {0}...'.format(name), dim=True)
                dumpFile = get_strings(raw_dumps_dir, name) 

                if dumpFile is None:
                    click.secho('Aborting analysis', fg='red')
                    return

                time = name.split('_')[-1].split('.')[0] #getting timestamp - timeformatting taken care of in class property
                action = name.split('_')[0].split('.')[0] #getting action

                f = open(dumpFile,mode='r')
                memdump = f.read()
                f.close()

                ft = ForensicTimelineElement() #initialize a finding
                ft.time = time
                ft.event=action
                ft.memdump=memdump
                findings.append(ft)

                if len(suspdata) >0:
                    susp_findings.extend(checksuspiciousmatch(memdump.splitlines(), suspdata, time, action))               
                
    now = datetime.now()
    timestamp = datetime.timestamp(now)
    filtered_out_path=os.path.join(dir,'filtered_output')
    os.makedirs(filtered_out_path, exist_ok=True)

    with open(os.path.join(dir,'filtered_output', 'forensic_timeline'+str(timestamp)+'.jsonl'),'a') as fp:  
        for f in findings:
            w = Writer()
            w.message=f.event
            w.datetime=f.time.isoformat()
            w.suspicious_string = f.memdump
            w.timestamp_desc="time_created"
            json.dump(w.__dict__,fp)
            fp.write('\n')

    with open(os.path.join(dir,'filtered_output', 'matches_forensic_timeline'+str(timestamp)+'.jsonl'),'a') as fp:  
        if len(susp_findings) >0:
            for f in susp_findings:
                w = Writer()
                w.matched_string=f.matched_string
                w.message=f.event
                w.datetime=f.time.isoformat()
                w.suspicious_string = f.suspicious_string
                w.timestamp_desc="time_created"
                json.dump(w.__dict__,fp)
                fp.write('\n')