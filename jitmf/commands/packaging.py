import click
import os
import lzma
import delegator
import json
import fileinput
import shutil
import requests
from subprocess import list2cmdline

from jitmf.utils.android import get_device_arch, inject_permissions, _determine_smali_path, _get_launchable_activity, _copy_meta_inf, execute_adb_command, modify_extract_libs
from jitmf.utils.patch_smali import _patch_smali_with_load_library, _revalue_locals_count, _patch_smali_official_app_flag
from jitmf.utils.Case import Case

delegator_timeout=120
FRIDA_CONFIG_FILENAME = 'libfrida-gadget.config.so'
FRIDA_CONFIG_LISTEN_FILENAME = 'libfrida-gadget.config.so_listen'
BASE_DIR = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
ASSETS_DIR = os.path.join(BASE_DIR,'utils','assets')
STABLE_FRIDA_VERSION = '12.7.24'
extstore='/sdcard'
extstore_signal='/sdcard/Android/data/org.thoughtcrime.securesms/files'

def download_gadget(case, arch, vfrida):
    frida_gh_json = requests.get('https://api.github.com/repos/frida/frida/releases/latest').json()
    frida_version = vfrida

    if not frida_version :
        frida_version=STABLE_FRIDA_VERSION
    elif frida_version=='LATEST':
        frida_version = frida_gh_json['tag_name']
        click.secho('> Getting latest version of Frida gadget identified as {0}'.format(frida_version), fg='green', bold=True)
    
    click.secho('> Using frida gadget version {0}'.format(frida_version), fg='green', bold=True)
    download_url='https://github.com/frida/frida/releases/download/'+frida_version+'/frida-gadget-'+frida_version+'-android-'+arch.split('-')[0] +'.so.xz'

    library = requests.get(download_url, stream=True)

    # overriding bad user-input
    if library.status_code==404 and vfrida!=None:
        frida_version = frida_gh_json['tag_name']
        click.secho('> Getting latest version of Frida gadget identified as {0}'.format(frida_version), fg='green', bold=True)
        library = requests.get(download_url, stream=True)
    
    if library.status_code==404:
        click.secho('> Something wrong in the download URL {0}'.format(download_url), fg='red', bold=True)
        exit

    library_destination=''

    library_destination=os.path.join(case.tmp_path, 'android', arch)
    os.makedirs(library_destination, exist_ok=True)
    
    click.secho(library_destination)
    # save the requests stream to file                       
    with open(os.path.join(library_destination,'libfrida-gadget.so.xz'), 'wb') as f:
        click.secho('Downloading {0} library to {1}...'.format(download_url,library_destination), fg='green', dim=True)
        f.write(library.content)

    return library_destination

def unpack_gadget(path: str):
    with lzma.open(os.path.join(path,'libfrida-gadget.so.xz')) as f:
        with open(os.path.join(path,'libfrida-gadget.so'),'wb') as g:
            g.write(f.read())
    
    return os.path.join(path,'libfrida-gadget.so')

def unpack_apk(case, apk_src_path: str):
    click.secho('Unpacking {0}'.format(apk_src_path), dim=True)

    decompiled_apk_dir = os.path.join(case.tmp_path,case.apk_name+'_decompiled')

    o = delegator.run(list2cmdline([
        'apktool',
        'decode',
        '--use-aapt2',
        '-f',
        '-o',
        decompiled_apk_dir,
        apk_src_path
    ]), timeout=delegator_timeout)

    if len(o.err) > 0:
        click.secho('An error may have occurred while extracting the APK.', fg='red')
        click.secho(o.err, fg='red')
    return decompiled_apk_dir

def inject_gadget(case: Case, apk_source: str):
 
    if case.hook_script_path != 'LISTEN':
        with open(case.hook_script_path) as f:
            first_line = f.readline()
            if first_line.startswith("// [ACTIVITY_OF_INTEREST]="):
                activity = first_line.split("// [ACTIVITY_OF_INTEREST]=")[1].strip()
            else:
                activity = _get_launchable_activity(apk_source) # getting the "main" activity which launches the app
    else:
        activity = _get_launchable_activity(apk_source) # getting the "main" activity which launches the app

    activity_class = _determine_smali_path(activity, case.decompiled_apk_dir) # locating its (above comment) smali in the dir
  
   
    with open(activity_class,'r') as f:
        smali_lines = f.readlines()

    if "telegram" in case.apk_name:
        inject_marker = [i for i, x in enumerate(smali_lines) if '.method static constructor <clinit>()V' in x]
    else:
        inject_marker = [i+1 for i, x in enumerate(smali_lines) if '# direct methods' in x]

    inject_marker = inject_marker[0]

    patched_smali = _patch_smali_with_load_library(smali_lines, inject_marker)
    patched_smali = _revalue_locals_count(patched_smali, inject_marker)

    click.secho('Writing patched smali back to: {0}'.format(activity_class), dim=True)

    with open(activity_class, 'w') as f:
        f.write(''.join(patched_smali))

def add_frida_files_to_libs(case: Case, architecture: str, gadget_source: str):
    """
        Copies a frida gadget for a specific architecture to
        an extracted APK's lib path.
        :param architecture:
        :param gadget_source:
        :return:
    """

    libs_path = os.path.join(case.decompiled_apk_dir, 'lib', architecture)

    # check if the libs path exists
    if not os.path.exists(libs_path):
        click.secho('Creating library path: {0}'.format(libs_path), dim=True)
        os.makedirs(libs_path)

    click.secho('Copying Frida gadget to libs path...', fg='green', dim=True)
    shutil.copyfile(gadget_source, os.path.join(libs_path, 'libfrida-gadget.so'))

    if case.hook_script_path !='LISTEN':
        click.secho('Copying Frida script to libs path...'+case.hook_script_path, fg='green', dim=True)
        shutil.copyfile(case.hook_script_path, os.path.join(libs_path, 'lib'+os.path.split(case.hook_script_path)[-1])+'.so') # CHECK HERE

def build_new_apk(case: Case):
    """
        Build a new .apk with the frida-gadget patched in.
        :return:
    """
    click.secho('Rebuilding the APK with the frida-gadget loaded...', fg='green', dim=True)
    o = delegator.run(list2cmdline([
        'apktool',
        '--use-aapt2',
        'build',
        case.decompiled_apk_dir,
        '-o',
        os.path.join(case.tmp_path,case.apk_name.replace('.apk', '.patched.apk')),
    ]), timeout=delegator_timeout)

    if len(o.err) > 0:
        click.secho('Rebuilding the APK may have failed. Read the following output to determine if apktool actually had an error: \n', fg='red')
        click.secho(o.err, fg='red')

    # _copy_meta_inf(case)
    click.secho('Built new APK with injected loadLibrary and frida-gadget', fg='green')

def sign_apk(case: Case):
    """
        Signs an APK with the objection key.
        The keystore itself was created with:
            keytool -genkey -v -keystore objection.jks -alias objection -keyalg RSA -keysize 2048 -validity 3650
            pass: basil-joule-bug
        :return:
    """

    click.secho('Signing new APK.', dim=True)
    # jarsigner -sigalg SHA1withRSA -digestalg SHA1 -tsa http://timestamp.digicert.com -storepass basil-joule-bug -keystore /jit-mf/jitmf/utils/assets/objection.jks org.telegram.messenger.modified.apk objection
    o = delegator.run(list2cmdline([
        'jarsigner',
        '-sigalg',
        'SHA1withRSA',
        '-digestalg',
        'SHA1',
        '-tsa',
        'http://timestamp.digicert.com',
        '-storepass',
        'basil-joule-bug',
        '-keystore',
        os.path.join(ASSETS_DIR,'objection.jks'),
        os.path.join(case.tmp_path,case.apk_name.replace('.apk', '.patched.apk')),
        'objection'
    ]))

    if len(o.err) > 0 or 'jar signed' not in o.out:
        click.secho('Signing the new APK may have failed.', fg='red')
        click.secho(o.out, fg='yellow')
        click.secho(o.err, fg='red')

    click.secho('Signed the new APK', fg='green')

def zipalign_apk(case: Case):
    """
        Performs the zipalign command on an APK.
        :return:
    """

    click.secho('Performing zipalign', dim=True)

    o = delegator.run(list2cmdline([
        'zipalign',
        '-v',
        '-p',
        '4',
        os.path.join(case.tmp_path,case.apk_name.replace('.apk', '.patched.apk')),
        os.path.join(case.tmp_path,case.apk_name.replace('.apk', '.patched.aligned.apk'))
    ]))

    if len(o.err) > 0:
        click.secho('Zipaligning the APK may have failed. Read the following output to determine if zipalign actually had an error: \n', fg='red')
        click.secho(o.err, fg='red')

    click.secho('Zipalign completed', fg='green')

#apktool cannot decompile private resources which telegram has
def rewrite_telegram_resources_v8(decompiled_apk_dir: str):
    folder_path = os.path.join(decompiled_apk_dir,'res/xml-v22')
    click.secho('** Rewriting Telegram resources'.format(folder_path))
    if os.path.exists(folder_path):
        delegator.run(list2cmdline([
            'rm',
            '-rf',
            folder_path
        ]), timeout=delegator_timeout)
    
    values_v31 = os.path.join(decompiled_apk_dir,'res/values-v31/dimens.xml')
    with fileinput.FileInput(values_v31, inplace=True) as file:
        for line in file:
            print(line.replace("@android:dimen/", "@*android:dimen/"), end='')
        
#removes the ~/.jitmf dir with tmp files used to repackage
def patch_android_apk(case: Case, source: str, vfrida: str=None, dest_dir: str=None):

    #run adb to get cpu arch of device
    click.secho('+ Getting device architecture')

    arch = get_device_arch(case)
    case.device_arch = arch
   
    if len(arch) <= 0:
        click.secho('Failed to determine architecture. Is the device connected and authorized?', fg='red', bold=True)
        return
    
    click.secho('> Device architecture identified as: {0}'.format(arch), fg='green', bold=True)

    frida_lib_path = download_gadget(case, arch, vfrida)
    frida_gadget_path = unpack_gadget(frida_lib_path)

    case.frida_gadget_path = frida_gadget_path
    permissions=['sd_read', 'sd_write']

    if case.hook_script_path=='LISTEN': #listenmode
        config_path = os.path.join(ASSETS_DIR,FRIDA_CONFIG_LISTEN_FILENAME)
        permissions.append('internet')
    else:
        config_path, case = rewrite_config_file(case)

    # unpack apk
    case.decompiled_apk_dir = unpack_apk(case, source)
    inject_permissions(case,permissions)
    modify_extract_libs(case) # <--- returning API_id_published_flood PROBLEM sometimess
    inject_gadget(case, source)
    add_frida_files_to_libs(case, arch, frida_gadget_path) 
    
    shutil.copy(os.path.join(config_path),os.path.join(case.decompiled_apk_dir,'lib',arch,FRIDA_CONFIG_FILENAME))

    if "telegram" in case.apk_name:
        rewrite_telegram_resources_v8(case.decompiled_apk_dir)
        _patch_smali_official_app_flag(case.decompiled_apk_dir)  #required because telegram has a flag that checks if the app is official through many means, including checking previous past sessions. Otherwise it show API_ID_FLOOD_PUBLISHED

    build_new_apk(case)
    sign_apk(case)
    zipalign_apk(case)

    if dest_dir is None:
        destination = source.replace('.apk', '.jitmf.apk')
    else:
        destination = os.path.join(dest_dir,case.apk_name.replace('.apk', '.jitmf.apk'))

    click.secho('Copying final apk from {0} in current directory to {1}...'.format(os.path.join(case.tmp_path,case.apk_name.replace('.apk', '.patched.aligned.apk')), destination))
    if os.path.exists(destination):
        click.secho('Removing existing jitmf apk in destination path', fg='red')
        os.remove(destination)

    shutil.copyfile(os.path.join(case.tmp_path,case.apk_name.replace('.apk', '.patched.aligned.apk')), destination)
    
    #cleanup()
    click.secho('Cleaning up...', fg='yellow')
    delegator.run(list2cmdline([
        'rm',
        '-rf',
        case.tmp_path
    ]), timeout=delegator_timeout)
    #download latest and store locally
    return destination, case

def rewrite_config_file(case: Case):
    with open(os.path.join(ASSETS_DIR,FRIDA_CONFIG_FILENAME),'r') as json_file:
        data=json.load(json_file)
        case.device_hook_dir = data['interaction']['path']
        hook_script_name = 'lib'+os.path.split(case.hook_script_path)[-1]+'.so'
        data['interaction']['path'] = os.path.join(data['interaction']['path'], hook_script_name)
        tmp_frida_config = os.path.join(case.tmp_path,FRIDA_CONFIG_FILENAME)
        with open(tmp_frida_config, 'w') as outfile:
            json.dump(data, outfile)
            return tmp_frida_config, case

# DOC: uninstall works assuming apk name is 'package_name.apk' e.g. com.pushbullet.android.apk
def deploy_to_device(apk_path: str, case: Case):
    
    o = execute_adb_command('adb -s '+case.attached_device_id+' uninstall '+os.path.splitext(case.apk_name)[0], case) # [0] - removes .apk extension)

    if o.out.strip()=='Success':
        click.secho('> Uninstalled original app', fg='green', bold=True)
    else:
        click.secho('> Could not uninstall original app.', fg='red', bold=True)

    o = execute_adb_command('adb  -s '+case.attached_device_id+' install '+apk_path, case)
    print(o.out.strip())

    if case.hook_script_path!='LISTEN':
        if not 'Success' in o.out.strip():
            click.secho('> Something went wrong while installing app', fg='red', bold=True)
            click.secho(o.out.strip())
            exit
    else:
        click.secho('> Instrumented app installed on device', fg='green', bold=True)

    #create log dir
    if "thoughtcrime" in case.apk_name:
        o = execute_adb_command('adb -s '+case.attached_device_id+' shell mkdir -p '+extstore_signal+'/jitmflogs', case) # [0] - removes .apk extension)
    else:
        o = execute_adb_command('adb -s '+case.attached_device_id+' shell mkdir -p '+extstore+'/jitmflogs', case) # [0] - removes .apk extension)

