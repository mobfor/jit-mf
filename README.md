## JIT MF

*Aim*: Create a tool that takes as input an `apk` file and produces a forensically enhanced apk which can dump elusive evidence of an attack from memory

Running on local
> $ python3 -m venv env

> $ source env/bin/activate

> $ python3 -m pip install --editable .

> $ deactivate
--------------------------------------------


This tool is used as part of [MobFor](https://gitlab.com/mobfor/mobfor-project) as part of the LOCARD EU Horizon 2020 Project. Full documentation is available [here](https://mobfor.gitlab.io/mobfor-pages/#/).

Influenced by https://github.com/sensepost/objection

[jks](https://gitlab.com/mobfor/jit-mf/-/blob/master/jitmf/utils/assets/objection.jks) key used to sign APKs may be changed as needed. Current one is copied from objection.

JIT-MF Drivers can be found [here](https://gitlab.com/mobfor/jit-mf/-/tree/master/resources/drivers) and may be updated (or more may be added), depending on the scenario.
