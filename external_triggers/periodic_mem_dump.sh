#! /bin/bash

VAR1="dump"

for i in {1..300}
do
    VAR2=$VAR1$i 
    # rm -rf $VAR2
    mkdir ./dumps/$VAR2
    echo -n "This is a test in loop $i ";
    python3 fridump.py -U Gadget -s -o ./dumps/$VAR2 &
    sleep 20
done